use na::{Vector3, Translation3};
use kiss3d::scene::SceneNode;
use kiss3d::window::Window;

use super::animal::*;

pub struct Drawer {
    nodes: Vec<SceneNode>,
    shadows: Vec<SceneNode>,
}

impl Drawer {
    pub fn setup(win: &mut SceneNode, num_nodes: usize) -> Drawer {
        Drawer {
            nodes: (0..num_nodes).map(|_| {win.add_sphere(0.3)}).collect(),
            shadows: (0..num_nodes).map(|_| {
                let mut c = win.add_cylinder(0.3, 0.01);
                c.set_color(0.0, 1.0, 0.0);
                c
            }).collect(),
        }
    }

    pub fn update(&mut self, animal: &Animal, win: &mut Window) {
        for (i, node) in animal.nodes.iter().enumerate() {
            self.nodes[i].set_local_translation(Translation3 {vector: node.pos});

            let mut sh = node.pos;
            sh.y = 0.0;
            self.shadows[i].set_local_translation(Translation3 {vector: sh});

            let point0 = node.pos.into();
            let mut point1: na::Point3<_> = node.pos.into();
            point1.y = 0.0;
            win.draw_line(&point0, &point1, &Vector3::new(0.0, 1.0, 1.0).into());
        }

        for edge in animal.edges.iter() {
            let point0 = animal.nodes[edge.conn.0].pos.into();
            let point1 = animal.nodes[edge.conn.1].pos.into();
            win.draw_line(&point0, &point1, &Vector3::new(edge.force, edge.len * 10.0, edge.len * 0.1).into());
        }
    }
}
