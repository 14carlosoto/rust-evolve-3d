extern crate kiss3d;
extern crate nalgebra as na;

#[macro_use]
extern crate serde_derive;

mod animal;

mod draw;
use self::draw::*;

mod constants;

mod generation;

use na::{Point3};
use kiss3d::window::Window;
use kiss3d::camera::{ArcBall};
use atomicwrites::{AtomicFile,AllowOverwrite};
use std::io::Write;

fn main() {
    let num = 6;

    let mut generation = match std::fs::read_to_string("generation.json") {
        Err(_) => generation::Generation::random(50, num, constants::Physics::default()),
        Ok(a) => serde_json::from_str(&a).unwrap(),
    };

    if std::env::args().len() == 1 {
        loop {
            generation.step();
            let af = AtomicFile::new("generation.json", AllowOverwrite);
            af.write(|f| {
                    f.write_all(serde_json::to_string(&generation).unwrap().as_bytes())
            }).unwrap();
        }
    } else {
        let mut window = Window::new("Evolve");
        let mut c = window.add_sphere(0.1);
        c.set_color(1.0, 0.0, 0.0);

        let eye = Point3::new(20.0f32, 20.0, 20.0);
        let at = Point3::origin();
        let mut first_person = ArcBall::new(eye, at);
        window.render_with_camera(&mut first_person);

        let mut node = window.add_group();
        let mut drawer = Drawer::setup(&mut node, num);

        let mut animal = generation.best().birth();
        for _ in 1..20000 {
            if !window.render_with_camera(&mut first_person) {
                break;
            }
            if !animal.update(&generation.phys) { break; }
            drawer.update(&animal, &mut window);
        }
    }
}
