use super::animal::genome::Genome;
use rand::Rng;
use rand::FromEntropy;
use rand_pcg::Mcg128Xsl64 as Prng;

type Scalar = f32;

#[derive(Serialize, Deserialize)]
pub struct Generation {
    genomes: Vec<(Genome, Scalar)>,
    generation: usize,
    pub phys: crate::constants::Physics,
    pub rng: Prng,
}

impl Generation {
    pub fn random(size: usize, animal_size: usize, phys: crate::constants::Physics) -> Self {
        let mut rng = Prng::from_entropy();
        let mut ans: Vec<_> = (0..size).map(|i| {
            let genome = super::animal::genome::Genome::random(animal_size, &mut rng);
            let fit = genome.fitness(&phys);
            println!("{}th animal, fitness ({})", i, fit);
            (genome, fit)
        }).collect();

        ans.sort_by(|(_, a), (_, b)| b.partial_cmp(&a).unwrap());
        Generation {
            genomes: ans,
            generation: 0,
            phys: phys,
            rng: rng,
        }
    }

    pub fn best(&self) -> &Genome {
        &self.genomes[0].0
    }

    pub fn step(&mut self) {
        println!("best fitness reached for generation {} is {}", self.generation, self.genomes[0].1);
        for i in 0..(self.genomes.len() - 1) {
            let k = self.rng.gen_range(i + 1, self.genomes.len());
            self.genomes[k].0 = self.genomes[i].0.clone();
        }

        for i in 0..self.genomes.len() {
            self.genomes[i].0.mutate(0.00001 + 0.0001 * i as f32, &mut self.rng);
            self.genomes[i].1 = self.genomes[i].0.fitness(&self.phys);
        }
        self.genomes.sort_by(|(_, a), (_, b)| b.partial_cmp(&a).unwrap());
        self.generation += 1;
    }
}
