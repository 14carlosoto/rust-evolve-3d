use na::Vector3;

mod brain;
use self::brain::*;


type Scalar = f32;
type Point = Vector3<Scalar>;

pub mod genome;

#[derive(Serialize, Deserialize, Clone)]
pub struct Node {
    pub wei: Scalar,
    pub pos: Point,
    pub vel: Point,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Edge {
    pub force: Scalar,
    pub len: Scalar,
    pub conn: (usize, usize),
}

pub struct Animal<'a> {
    pub nodes: Vec<Node>,
    pub edges: Vec<Edge>,
    pub brain: &'a Brain,
    fitness: Scalar,
}

impl<'a> Animal<'a> {
    pub fn update(&mut self, phys: &crate::constants::Physics) -> bool {
        let ys = self.edges.iter().map(|x|
            (self.nodes[x.conn.0].pos - self.nodes[x.conn.1].pos).norm()).collect();
        let outs = self.brain.simulate(ys);
        for i in 0..outs.len() {
            self.edges[i].len = (1.0 + outs[i]) * 5.0;
        }

        for edge in self.edges.iter() {
            let a = edge.conn.0;
            let b = edge.conn.1;
            let diff = self.nodes[a].pos - self.nodes[b].pos;
            let factor = edge.force * (edge.len - diff.norm());
            self.nodes[a].vel += diff * factor * phys.timestep * phys.force;
            self.nodes[b].vel -= diff * factor * phys.timestep * phys.force;
        }

        let mut val = Vector3::new(0.0, 0.0, 0.0);

        for i in 0..self.nodes.len() {
            let mut acc = Vector3::new(0.0, 0.0, 0.0);
            let node = &mut self.nodes[i];
            let pos = &mut node.pos;
            let vel = &mut node.vel;

            acc -= phys.up * phys.gravity;

            *vel *= phys.air_friction.powf(phys.timestep);
            *vel += acc * phys.timestep;
            // acc = zero();


            *pos += phys.timestep * &*vel;
            // let borrow = self.nodes[i].vel.clone();
            // self.nodes[i].pos += phys.timestep * borrow;
            if pos.dot(&phys.up) < phys.floor {
                *pos += phys.up * (phys.floor - pos.dot(&phys.up));
                *vel *= phys.ground_friction.powf(phys.timestep);
            }

            // plane.y = 0.0;
            val += &*pos - &phys.up * phys.floor;
        }
        // println!("{}", val.y);
        let collapsed = val.dot(&phys.up) <= 0.01;
        val.y = 0.0;

        let val = val.norm();

        if self.fitness < val {
            self.fitness = val
        }

        !collapsed
    }
}
