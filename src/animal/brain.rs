use rand::Rng;
use rand::distributions::uniform::Uniform;
use rand::distributions::StandardNormal;
type Scalar = f32;

fn activation(s: Scalar) -> Scalar {
    s.tanh()
}


#[derive(Serialize, Deserialize, Clone)]
pub struct Brain {
    pub layers: Vec<usize>,
    pub weis: Vec<Vec<Vec<Scalar>>>,
}

impl Brain {
    pub fn simulate(&self, inp: Vec<Scalar>) -> Vec<Scalar> {
        let mut curr = inp;
        for i in 1..self.layers.len() {
            let mut new = vec![0.0; self.layers[i]];
            for j in 0..self.layers[i-1] {
                for k in 0..self.layers[i] {
                    new[k] += curr[j] * self.weis[i-1][j][k];
                }
            }

            for k in 0..self.layers[i] {
                new[k] = activation(new[k]);
            }

            curr = new;
        }

        curr
    }

    pub fn random<R: Rng>(layers: Vec<usize>, rng: &mut R) -> Brain {
        let mut weis = Vec::new();
        let range = Uniform::new(-2.0, 2.0);
        for i in 1..layers.len() {
            weis.push((0..layers[i-1]).map(|_| (0..layers[i]).map(|_| rng.sample(range)).collect()).collect());
        }

        Brain {
            layers,
            weis,
        }
    }

    pub fn evolve<R: Rng>(&mut self, xi: Scalar, rng: &mut R) {
        for i in 1..self.layers.len() {
            for j in 0..self.layers[i-1] {
                for k in 0..self.layers[i] {
                    self.weis[i-1][j][k] += xi * rng.sample(StandardNormal) as f32;
                }
            }
        }
    }
}
