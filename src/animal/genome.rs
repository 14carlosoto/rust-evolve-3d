use super::Scalar;
use super::Point;

use rand::Rng;
use rand::distributions::uniform::Uniform;
use rand::distributions::StandardNormal;
use nalgebra::Vector3;

use crate::animal::Animal;

#[derive(Serialize, Deserialize, Clone)]
struct Node {
    pos: Point,
    wei: Scalar,
}

#[derive(Serialize, Deserialize, Clone)]
struct Edge {
    force: Scalar,
    conn: (usize, usize),
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Genome {
    nodes: Vec<Node>,
    edges: Vec<Edge>,
    brain: super::brain::Brain,
}

impl Genome {
    pub fn random<R: Rng>(size: usize, rng: &mut R) -> Genome {
        let range = Uniform::new(0.0, 1.0);
        let nodes: Vec<Node> = (0..size).map(|_| Node {
            wei: 0.0,
            pos: Vector3::new(
                rng.sample(range) * 10.0 - 5.0,
                rng.sample(range) * 10.0 - 5.0,
                rng.sample(range) * 10.0 - 5.0,
            ),
        }).collect();
        let edges: Vec<Edge> = (0..size)
            .flat_map(|i| (0..i)
                      .map(move |j| (i, j)))
            .map(|(i, j)| {
           let i = i.clone();
            Edge {
                force: 10.0,
                conn: (i, j),
            }
        }).collect();

        let brain = super::brain::Brain::random(vec![edges.len(), size, size, edges.len()], rng);
        Genome {
            nodes,
            edges,
            brain,
        }
    }

    pub fn mutate<R: Rng>(&mut self, xi: Scalar, rng: &mut R) {
        let bell = StandardNormal;
        let mut total = Vector3::new(0.0, 0.0, 0.0);
        for n in self.nodes.iter_mut() {
                n.pos.x += rng.sample(bell) as f32 * xi as f32;
                n.pos.y += rng.sample(bell) as f32 * xi as f32;
                n.pos.z += rng.sample(bell) as f32 * xi as f32;

                if n.pos.norm() >= 12.0 {
                    n.pos /= n.pos.norm() * 12.0;
                }

                if n.pos.y < 1.0 {
                    n.pos.y = 1.0;
                }

                total += n.pos;
        }

        total /= self.nodes.len() as f32;
        for n in self.nodes.iter_mut() {
            n.pos.x -= total.x;
            n.pos.z -= total.z;
        }

        self.brain.evolve(xi, rng);
    }

    pub fn birth<'a>(&'a self) -> super::Animal<'a> {
        fn node(x: &super::genome::Node) -> super::Node {
            super::Node {
                vel: ::nalgebra::Vector3::new(0.0, 0.0, 0.0),
                pos: x.pos,
                wei: x.wei,
            }
        }
        fn edge(x: &crate::animal::genome::Edge) -> super::Edge {
            super::Edge {
                len: 0.0,
                force: x.force,
                conn: x.conn
            }
        }

        Animal {
            nodes: self.nodes.iter().map(node).collect(),
            edges: self.edges.iter().map(edge).collect(),
            brain: &self.brain,
            fitness: 0.0,
        }
    }

    pub fn fitness(&self, phys: &crate::constants::Physics) -> Scalar {
        let mut animal = self.birth();
        for _ in 0..20000 {
            if !animal.update(phys) { break; }
        }

        animal.fitness
    }
}
